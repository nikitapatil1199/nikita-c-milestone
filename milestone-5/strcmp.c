#include <stdio.h>
#include <string.h>

int str_compare(char *str1,char *str2)
{
	if(strcmp(str1, str2))
		return -1;
	else{
		if(strcmp(str2, str1))
			return 1;
	}
	else{
		return 0;
	}
}

int main()
{
    char str1[] = "abcd", str2[] = "abCd";
    int result;

   
    result = str_compare(str1, str2);
    printf("strcmp(str1, str2) = %d\n", result);

    return 0;
}